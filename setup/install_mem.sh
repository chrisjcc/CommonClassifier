action() {
	if [ -e "${CMSSW_BASE}" ]; then
		local origin="$( pwd )"

		# source this script to install the MEM package
		cd $CMSSW_BASE

		# make the MEM install dir
		mkdir -p src/TTH
		cd src/TTH

		# get the MEM code
		git clone https://gitlab.cern.ch/jpata/MEIntegratorStandalone.git MEIntegratorStandalone --branch v0.4

		# copy the OpenLoops ME libraries
		cp -R MEIntegratorStandalone/libs/* ../../lib/$SCRAM_ARCH/

		eval `scramv1 runtime -sh`
		ls
		scram setup MEIntegratorStandalone/deps/gsl.xml

		cd $origin
	fi
}
action "$@"
